/**
 * @author Guillermo B Díaz Solís
 * @since 3 de jul. de 2016
 */
package com.guillermods.smartcard.service.impl;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.guillermods.smartcard.dao.SmartCardDao;
import com.guillermods.smartcard.dao.impl.SmartCardAthenaDaoImpl;
import com.guillermods.smartcard.exception.BadPinException;
import com.guillermods.smartcard.exception.BlockedPinException;
import com.guillermods.smartcard.exception.DeviceNotFoundException;
import com.guillermods.smartcard.exception.DriverNotFoundException;
import com.guillermods.smartcard.model.CertificateDto;
import com.guillermods.smartcard.service.SmartCardService;

/**
 * @author Guillermo
 *
 */
public class AthenaSmartCardServiceImplTest {

	private SmartCardDao smartCardDao;
	private SmartCardService smartCardService;
	
	@Before
	public void setUp(){
		smartCardDao = new SmartCardAthenaDaoImpl();
		smartCardService = new SmartCardAthenaServiceImpl(smartCardDao);
	}
	
	/**
	 * Test method for {@link com.guillermods.smartcard.service.impl.AthenaSmartCardServiceImpl#getCertificates(java.lang.String)}.
	 */
	@Test
	public void testGetCertificates() {
		try {
			List<CertificateDto> certs = smartCardService.getCertificates("1189");
			for (CertificateDto certificateDto : certs) {
				System.out.println(certificateDto);
			}
			
		} catch (BadPinException | BlockedPinException | DeviceNotFoundException | DriverNotFoundException e) {
			e.printStackTrace();
		}
	}

}
