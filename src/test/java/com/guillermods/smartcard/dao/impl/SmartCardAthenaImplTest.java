/**
 * @author Guillermo B Díaz Solís
 * @since 3 de jul. de 2016
 */
package com.guillermods.smartcard.dao.impl;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.ProviderException;
import java.security.cert.CertificateException;
import java.util.Enumeration;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import com.guillermods.smartcard.dao.SmartCardDao;

/**
 * @author Guillermo
 *
 */
public class SmartCardAthenaImplTest {

	SmartCardDao smartCardDao;
	
	@Before
	public void setUp() {
		smartCardDao = new SmartCardAthenaDaoImpl();
		
	}
	
	@Test
	public void testGetCertificates(){
		KeyStore store = null;
		
		try {
			store = smartCardDao.openKeyStore("1189");
			Enumeration<String> aliases =  store.aliases();
			assertTrue(aliases.hasMoreElements());
			
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (ProviderException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		
	}

}
