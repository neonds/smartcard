/**
 * @author Guillermo B Díaz Solís
 * @since 27 de jun. de 2016
 */
package com.guillermods.smartcard.controller;

import java.util.List;

import com.guillermods.smartcard.dao.impl.SmartCardAthenaDaoImpl;
import com.guillermods.smartcard.exception.BadPinException;
import com.guillermods.smartcard.exception.BlockedPinException;
import com.guillermods.smartcard.exception.DeviceNotFoundException;
import com.guillermods.smartcard.exception.DriverNotFoundException;
import com.guillermods.smartcard.model.CertificateDto;
import com.guillermods.smartcard.service.SmartCardService;
import com.guillermods.smartcard.service.impl.SmartCardAthenaServiceImpl;

import spark.Request;
import spark.Response;
import spark.Route;

/**
 * @author Guillermo
 *
 */
public class SmartCardController implements Route {

	private SmartCardService smartCardService;

	/**
	 * 
	 */
	public SmartCardController() {
		super();

		smartCardService = new SmartCardAthenaServiceImpl(new SmartCardAthenaDaoImpl());
	}

	public static String POST = "/smartcard/certificates";

	/*
	 * (non-Javadoc)
	 * 
	 * @see spark.Route#handle(spark.Request, spark.Response)
	 */
	@Override
	public Object handle(Request request, Response response) {
		String pin = request.queryParams("pin");
		response.type("application/json");
		response.status(200);

		List<CertificateDto> certs = null;
		try {
			certs = smartCardService.getCertificates(pin);
		} catch (BadPinException e) {
			response.header("errorMsg", e.getMessage());
			response.status(401); // Unauthorized
			
		} catch (BlockedPinException e) {
			response.header("errorMsg", e.getMessage());
			response.status(423); //Locked (WebDAV; RFC 4918)
			
		} catch (DeviceNotFoundException e) {
			response.header("errorMsg", e.getMessage());
			response.status(404); //Not Found

		} catch (DriverNotFoundException e) {
			response.header("errorMsg", e.getMessage());
			response.status(500); //Internal Server Error
			
		}

		return certs;
	}

}
