/**
 * @author Guillermo B Díaz Solís
 * @since 3 de jul. de 2016
 */
package com.guillermods.smartcard.util;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

/**
 * @author Guillermo
 *
 */
public class PropertiesUtils {

	public static Properties loadProperties (String props){

	    final Properties p = new Properties();
	    try {
			p.load(new StringReader(props));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    return p;
	}
}
