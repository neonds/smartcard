/**
 * @author Guillermo B Díaz Solís
 * @since 3 de jul. de 2016
 */
package com.guillermods.smartcard.util;

import java.io.File;
import java.io.InputStream;

/**
 * @author Guillermo
 *
 */
public class FileUtils {

	private final static ClassLoader CLASS_LOADER = FileUtils.class.getClassLoader();
	
	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public static InputStream loadResource(String fileName){
		return CLASS_LOADER.getResourceAsStream(fileName);
	}
	
	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public static File loadFile(String filePath){
		return new File (CLASS_LOADER.getResource(filePath).getFile());
	}
	
	
}
