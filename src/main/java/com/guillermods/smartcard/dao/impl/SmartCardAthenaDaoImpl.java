/**
 * @author Guillermo B Díaz Solís
 * @since 3 de jul. de 2016
 */
package com.guillermods.smartcard.dao.impl;

import static com.guillermods.smartcard.util.OSValidator.isMac;
import static com.guillermods.smartcard.util.OSValidator.isSolaris;
import static com.guillermods.smartcard.util.OSValidator.isUnix;
import static com.guillermods.smartcard.util.OSValidator.isWindows;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.ProviderException;
import java.security.Security;
import java.security.cert.CertificateException;

import com.guillermods.smartcard.dao.SmartCardDao;
import com.guillermods.smartcard.util.FileUtils;

/**
 * Implemententación PCKS11 de Athena
 * @author Guillermo
 *
 */
public class SmartCardAthenaDaoImpl implements SmartCardDao {
	/**
	 * 
	 */
	public static final String PKCS11 = "PKCS11";
	static final String WINDOWS_PROPS = "smartcard/windows.properties";
	static final String LINUX_PROPS = "smartcard/linux.properties";
	static final String MAC_PROPS = "smartcard/mac.properties";

	public Provider loadProvider() throws ProviderException {
		// SunPKCS11 pkcs11Provider = null;

		InputStream inputStream = null;
		if (isWindows()) {
			inputStream = FileUtils.loadResource(WINDOWS_PROPS);
		} else if (isMac()) {
			inputStream = FileUtils.loadResource(MAC_PROPS);
		} else if (isUnix()) {
			inputStream = FileUtils.loadResource(LINUX_PROPS);
		} else if (isSolaris()) {

		} else {

		}

		Provider p = null;
		try {
			Class<?> c = Class.forName("sun.security.pkcs11.SunPKCS11");
			Constructor<?> constructor = c.getConstructor(InputStream.class);
			p = (Provider) constructor.newInstance(inputStream);
			Security.addProvider(p);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return p;
	}

	public KeyStore openKeyStore(String password)
			throws KeyStoreException, ProviderException, NoSuchAlgorithmException, CertificateException, IOException {

		KeyStore keyStore = null;

		keyStore = KeyStore.getInstance(PKCS11, loadProvider());
		keyStore.load(null, password.toCharArray());

		return keyStore;
	}
}
