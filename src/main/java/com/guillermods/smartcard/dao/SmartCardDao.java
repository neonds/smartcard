/**
 * @author Guillermo B Díaz Solís
 * @since 3 de jul. de 2016
 */
package com.guillermods.smartcard.dao;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.ProviderException;
import java.security.cert.CertificateException;

/**
 * @author Guillermo
 *
 */
public interface SmartCardDao  {


	public Provider loadProvider() throws ProviderException;

	public KeyStore openKeyStore(String password)
			throws KeyStoreException, ProviderException, NoSuchAlgorithmException, CertificateException, IOException;

}
