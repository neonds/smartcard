/**
 * @author Guillermo B Díaz Solís
 * @since 27 de jun. de 2016
 */
package com.guillermods.smartcard.exception;

public class DriverNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6241271652114772960L;
	
	public DriverNotFoundException() {
		super("Driver are not avaiable...");
	}
}
