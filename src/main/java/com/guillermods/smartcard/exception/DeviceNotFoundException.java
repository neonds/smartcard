/**
 * @author Guillermo B Díaz Solís
 * @since 27 de jun. de 2016
 */
package com.guillermods.smartcard.exception;

public class DeviceNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9008391684112422977L;

	public DeviceNotFoundException() {
		super("Reader or card not found!");
	}
}
