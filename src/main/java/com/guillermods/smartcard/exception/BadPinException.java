/**
 * @author Guillermo B Díaz Solís
 * @since 27 de jun. de 2016
 */
package com.guillermods.smartcard.exception;

public class BadPinException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3622960326311697086L;

	public BadPinException() {
		super("Pin Typed was wrong");
	}
}
