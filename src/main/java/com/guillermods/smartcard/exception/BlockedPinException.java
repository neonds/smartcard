/**
 * @author Guillermo B Díaz Solís
 * @since 27 de jun. de 2016
 */
package com.guillermods.smartcard.exception;

public class BlockedPinException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2130221276892246839L;

	public BlockedPinException() {
		super("Card is blocked");
	}
}
