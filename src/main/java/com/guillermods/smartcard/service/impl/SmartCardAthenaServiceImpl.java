/**
 * @author Guillermo B Díaz Solís
 * @since 27 de jun. de 2016
 */
package com.guillermods.smartcard.service.impl;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.ProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import com.guillermods.smartcard.dao.SmartCardDao;
import com.guillermods.smartcard.exception.BadPinException;
import com.guillermods.smartcard.exception.BlockedPinException;
import com.guillermods.smartcard.exception.DeviceNotFoundException;
import com.guillermods.smartcard.exception.DriverNotFoundException;
import com.guillermods.smartcard.model.CertificateDto;
import com.guillermods.smartcard.service.SmartCardService;
import com.guillermods.smartcard.util.PropertiesUtils;

/**
 * @author Guillermo
 */
public class SmartCardAthenaServiceImpl implements SmartCardService {

	private SmartCardDao smartCardDao;
	private KeyStore store;

	/**
	 * @param smartCardDao
	 */
	public SmartCardAthenaServiceImpl(SmartCardDao smartCardDao) {
		this.smartCardDao = smartCardDao;
	}

	/**
	 * 
	 */
	@Override
	public List<CertificateDto> getCertificates(String pin)
			throws BadPinException, BlockedPinException, DeviceNotFoundException, DriverNotFoundException {

		List<CertificateDto> certificates = new ArrayList<>();

		try {
			store = this.smartCardDao.openKeyStore(pin);
			Enumeration<String> aliases = store.aliases();

			while (aliases.hasMoreElements()) {
				String alias = aliases.nextElement();
				CertificateDto certificateDto = new CertificateDto();
				certificateDto.setAlias(alias);
				X509Certificate cert = (X509Certificate) store.getCertificate(alias);
				certificateDto.setEncodedCertificate(Base64.getEncoder().encodeToString(cert.getEncoded()));

				Properties props = PropertiesUtils.loadProperties(cert.getSubjectDN().getName().replace(",", "\n"));

				certificateDto.setName(props.getProperty("CN"));
				certificates.add(certificateDto);
			}
		} catch (KeyStoreException | NoSuchAlgorithmException | ProviderException | CertificateException
				| IOException e) {

			if (e.getMessage().contains("Library") && e.getMessage().contains("does not exist")) {
				throw new DriverNotFoundException();
			}
			if (e.getMessage().equals("PKCS11 not found")) {

				throw new DeviceNotFoundException();
			}
			if (e.getMessage().equals("load failed")) {

				
				Throwable trow = e.getCause();
				while (trow != null){
					
					if(trow.toString().contains("CKR_PIN_LOCKED")){
						throw new BlockedPinException();
					}
					
					trow = trow.getCause();
				}
				throw new BadPinException();
			}

		}

		return certificates;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.guillermods.smartcard.service.SmartCardService#close()
	 */
	@Override
	public void close() {

	}

}
