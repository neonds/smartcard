/**
 * @author Guillermo B Díaz Solís
 * @since 27 de jun. de 2016
 */
package com.guillermods.smartcard.service;

import java.util.List;

import com.guillermods.smartcard.exception.BadPinException;
import com.guillermods.smartcard.exception.BlockedPinException;
import com.guillermods.smartcard.exception.DeviceNotFoundException;
import com.guillermods.smartcard.exception.DriverNotFoundException;
import com.guillermods.smartcard.model.CertificateDto;

public interface SmartCardService {

	public List<CertificateDto> getCertificates(String pin)
			throws BadPinException, BlockedPinException, DeviceNotFoundException, DriverNotFoundException;
	
	public void close();
}
