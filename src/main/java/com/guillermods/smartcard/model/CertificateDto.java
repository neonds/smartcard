/**
 * @author Guillermo B Díaz Solís
 * @since 3 de jul. de 2016
 */
package com.guillermods.smartcard.model;

/**
 * @author Guillermo
 *
 */
public class CertificateDto {

	private String name;
	private String alias;
	private String encodedCertificate;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getEncodedCertificate() {
		return encodedCertificate;
	}
	public void setEncodedCertificate(String encodedCertificate) {
		this.encodedCertificate = encodedCertificate;
	}
	@Override
	public String toString() {
		return "CertificateDto [name=" + name + ", alias=" + alias + ", encodedCertificate=" + encodedCertificate + "]";
	}


	

	
	
	

	
	
}
