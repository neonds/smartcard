/**
 * @author Guillermo B Díaz Solís
 * @since 3 de jul. de 2016
 */
import static spark.Spark.before;
import static spark.Spark.options;
import static spark.Spark.post;

import com.guillermods.smartcard.controller.SmartCardController;
import com.guillermods.smartcard.util.JsonUtil;

public class Application {

	public static void main(String[] args) {


		options("/*", (request, response) -> {

			String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
			if (accessControlRequestHeaders != null) {
				response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
			}

			String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
			if (accessControlRequestMethod != null) {
				response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
			}

			return "OK";
		});

		before((request, response) -> {
			response.header("Access-Control-Allow-Origin", "*");
			
		});

		post(SmartCardController.POST, new SmartCardController(), JsonUtil.json());
	}

}
